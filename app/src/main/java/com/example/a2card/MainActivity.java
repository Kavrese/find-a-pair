package com.example.a2card;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
int click = 0,click1 = 0,click2 = 0,win = 0,turnI = 0;
View view1,view2;
ImageView card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,
    card11,card12,card13,card14,card15,card16,card17,card18,card19,card20,
    card21,card22,card23,card24,card25,card26,card27,card28,card29,card30,cup,restart;
ArrayList nums = new ArrayList();
TextView turn,text;
Boolean isTap = true;
Animation cup_anim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        restart = findViewById(R.id.restart);
        cup = findViewById(R.id.cup);
        cup_anim = AnimationUtils.loadAnimation(this,R.anim.cup_alpha_anim);
        nums = generationRandomList();
        text = findViewById(R.id.text);
        turn = findViewById(R.id.turn);
        card1 = findViewById(R.id.card1);
        card2 = findViewById(R.id.card2);
        card3 = findViewById(R.id.card3);
        card4 = findViewById(R.id.card4);
        card5 = findViewById(R.id.card5);
        card6 = findViewById(R.id.card6);
        card7 = findViewById(R.id.card7);
        card8 = findViewById(R.id.card8);
        card9 = findViewById(R.id.card9);
        card10 = findViewById(R.id.card10);
        card11 = findViewById(R.id.card11);
        card12 = findViewById(R.id.card12);
        card13 = findViewById(R.id.card13);
        card14 = findViewById(R.id.card14);
        card15 = findViewById(R.id.card15);
        card16 = findViewById(R.id.card16);
        card17 = findViewById(R.id.card17);
        card18 = findViewById(R.id.card18);
        card19 = findViewById(R.id.card19);
        card20 = findViewById(R.id.card20);
        card21 = findViewById(R.id.card21);
        card22 = findViewById(R.id.card22);
        card23 = findViewById(R.id.card23);
        card24 = findViewById(R.id.card24);
        card25 = findViewById(R.id.card25);
        card26 = findViewById(R.id.card26);
        card27 = findViewById(R.id.card27);
        card28 = findViewById(R.id.card28);
        card29 = findViewById(R.id.card29);
        card30 = findViewById(R.id.card30);

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
    }
    public ArrayList generationRandomList (){
        Random random = new Random();
        ArrayList arrayList = new ArrayList();
        ArrayList uses = new ArrayList();
        for (int l=0;l<999;l++){
            if(arrayList.size() == 30){
                break;
            }
            int num = random.nextInt(30)+1;
            if(uses.contains(num)){
                continue;
            }else{
                arrayList.add(num);
                uses.add(num);
            }
        }
        return arrayList;
    }

    public void onClick (View view){
        if(isTap) {
            click++;
            if (click == 1) {
                view1 = view;                       //Первая нажатая ячейка
                int command = onSwitchView(view);   //Определяем номер ячейки
                click1 = (int) nums.get(command);  //Определяем по номеру ячейки, какое число в ней
                refreshBack(click1, view);           //Меняем скин
            } else {
                click = 0;
                view2 = view;                         //Первая нажатая ячейка
                if (view1 == view2) {                    //Определяем не таже-ли это ячейка
                    click--;
                    Toast.makeText(this, "Выберите другую ячейку", Toast.LENGTH_SHORT).show();
                } else {
                    turnI++;
                    turn.setText(String.valueOf(turnI));
                    int command = onSwitchView(view);   //Определяем номер ячейки
                    click2 = (int) nums.get(command);  //Определяем по номеру ячейки, какое число в ней
                    refreshBack(click2, view);           //Меняем скин
                    if (((click1 % 2 != 0) && (click1 + 1 == click2)) || ((click1 % 2 == 0) && (click1 - 1 == click2))) {
                        view1.setVisibility(View.INVISIBLE);
                        view2.setVisibility(View.INVISIBLE);
                        win++;
                        if (win == 15) {
                            text.setVisibility(View.GONE);
                            turn.setVisibility(View.GONE);
                            Toast.makeText(this, "Вы выйграли ! За "+turnI+" ходов", Toast.LENGTH_SHORT).show();
                            cup.setVisibility(View.VISIBLE);
                            cup.startAnimation(cup_anim);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    restart.setVisibility(View.VISIBLE);
                                }
                            },3500);
                        }
                    } else {
                        isTap = false;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                view1.setBackgroundResource(R.drawable.maket_no);
                                view2.setBackgroundResource(R.drawable.maket_no);
                                isTap = true;
                            }
                        }, 500);
                    }
                }
            }
        }
    }
    public int onSwitchView (View view){
        int command = 0;
        switch (view.getId()){
            case R.id.card1:
                command = 1;
                break;
            case R.id.card2:
                command = 2;
                break;
            case R.id.card3:
                command = 3;
                break;
            case R.id.card4:
                command = 4;
                break;
            case R.id.card5:
                command = 5;
                break;
            case R.id.card6:
                command = 6;
                break;
            case R.id.card7:
                command = 7;
                break;
            case R.id.card8:
                command = 8;
                break;
            case R.id.card9:
                command = 9;
                break;
            case R.id.card10:
                command = 10;
                break;
            case R.id.card11:
                command = 11;
                break;
            case R.id.card12:
                command = 12;
                break;
            case R.id.card13:
                command = 13;
                break;
            case R.id.card14:
                command = 14;
                break;
            case R.id.card15:
                command = 15;
                break;
            case R.id.card16:
                command = 16;
                break;
            case R.id.card17:
                command = 17;
                break;
            case R.id.card18:
                command = 18;
                break;
            case R.id.card19:
                command = 19;
                break;
            case R.id.card20:
                command = 20;
                break;
            case R.id.card21:
                command = 21;
                break;
            case R.id.card22:
                command = 22;
                break;
            case R.id.card23:
                command = 23;
                break;
            case R.id.card24:
                command = 24;
                break;
            case R.id.card25:
                command = 25;
                break;
            case R.id.card26:
                command = 26;
                break;
            case R.id.card27:
                command = 27;
                break;
            case R.id.card28:
                command = 28;
                break;
            case R.id.card29:
                command = 29;
                break;
            case R.id.card30:
                command = 30;
                break;
        }
        return command-1;
    }
    public void refreshBack (int num,View view){
        switch (num){
            case 1:
            case 2:
                view.setBackgroundResource(R.drawable.maket_card_1);
                break;
            case 3:
            case 4:
                view.setBackgroundResource(R.drawable.maket_card_2);
                break;
            case 5:
            case 6:
                view.setBackgroundResource(R.drawable.maket_card_3);
                break;
            case 7:
            case 8:
                view.setBackgroundResource(R.drawable.maket_card_4);
                break;
            case 9:
            case 10:
                view.setBackgroundResource(R.drawable.maket_card_5);
                break;
            case 11:
            case 12:
                view.setBackgroundResource(R.drawable.maket_card_6);
                break;
            case 13:
            case 14:
                view.setBackgroundResource(R.drawable.maket_card_7);
                break;
            case 15:
            case 16:
                view.setBackgroundResource(R.drawable.maket_card_8);
                break;
            case 17:
            case 18:
                view.setBackgroundResource(R.drawable.maket_card_9);
                break;
            case 19:
            case 20:
                view.setBackgroundResource(R.drawable.maket_card_10);
                break;
            case 21:
            case 22:
                view.setBackgroundResource(R.drawable.maket_card_11);
                break;
            case 23:
            case 24:
                view.setBackgroundResource(R.drawable.maket_card_12);
                break;
            case 25:
            case 26:
                view.setBackgroundResource(R.drawable.maket_card_13);
                break;
            case 27:
            case 28:
                view.setBackgroundResource(R.drawable.maket_card_14);
                break;
            case 29:
            case 30:
                view.setBackgroundResource(R.drawable.maket_card_15);
                 break;
        }
    }
}
